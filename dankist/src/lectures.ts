// 'use strict';
// // Image Imports
// // @ts-ignore
// import Icon from './assets/img/icon.png';
// // @ts-ignore
// import Logo from './assets/img/logo.png';
// // JS Imports
// import accounts from './data';
// /////////////////////////////////////////////////
// /////////////////////////////////////////////////
// // BANKIST APP
//
//
// // Elements
// const labelWelcome = document.querySelector('.welcome');
// const labelDate = document.querySelector('.date');
// const labelBalance = document.querySelector('.balance__value');
// const labelSumIn = document.querySelector('.summary__value--in');
// const labelSumOut = document.querySelector('.summary__value--out');
// const labelSumInterest = document.querySelector('.summary__value--interest');
// const labelTimer = document.querySelector('.timer');
//
// const containerApp = document.querySelector('.app');
// const containerMovements = document.querySelector('.movements');
//
// const btnLogin = document.querySelector('.login__btn');
// const btnTransfer = document.querySelector('.form__btn--transfer');
// const btnLoan = document.querySelector('.form__btn--loan');
// const btnClose = document.querySelector('.form__btn--close');
// const btnSort = document.querySelector('.btn--sort');
//
// const inputLoginUsername = document.querySelector('.login__input--user');
// const inputLoginPin = document.querySelector('.login__input--pin');
// const inputTransferTo = document.querySelector('.form__input--to');
// const inputTransferAmount = document.querySelector('.form__input--amount');
// const inputLoanAmount = document.querySelector('.form__input--loan-amount');
// const inputCloseUsername = document.querySelector('.form__input--user');
// const inputClosePin = document.querySelector('.form__input--pin');
//
// /////////////////////////////////////////////////
// /////////////////////////////////////////////////
// // LECTURES
//
// // const currencies = new Map([
// //   ['USD', 'United States dollar'],
// //   ['EUR', 'Euro'],
// //   ['GBP', 'Pound sterling'],
// // ]);
// //
// // const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
//
// /////////////////////////////////////////////////
// // LOOPING ARRAYS
// // const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
// //
// // // METHOD #1 (Good but not as good as the forEach Loop) The "For of Loop"
// // console.log('----- For of Loop -----');
// // for (const [i, movement] of movements.entries()) {
// //   if (movement > 0) {
// //     console.log(`Movement ${i + 1}: You deposited ${movement}`);
// //   } else {
// //     console.log(`Movement ${i + 1}: You withdrew ${Math.abs(movement)}`);
// //   }
// // }
// //
// // // METHOD #2 (Ideal) The "forEach Loop"
// // console.log('----- forEach Loop -----');
// // // First parameter of forEach loop must always be the current element, the second the current index, and the third must be the array
// // movements.forEach((mov, // Current Element
// //                    i, // Current Index
// //                    arr // Current Array
// // ) => {
// //   if (mov > 0) {
// //     console.log(`Movement ${i + 1}: You deposited ${mov}`);
// //   } else {
// //     console.log(`Movement ${i + 1}: You withdrew ${Math.abs(mov)}`);
// //   }
// // });
// // 0: function(200)
// // 1: function(450)
// // 2: function(400)
// // ...
// // forEach with Maps and Sets
//
// // With a Map
// const currencies = new Map([
//   ['USD', 'United States dollar'],
//   ['EUR', 'Euro'],
//   ['GBP', 'Pound sterling']
// ]);
//
// currencies.forEach((val, key, map) => {
//   console.log(`${key}: ${val}`);
// });
//
// // With a Set
// const currenciesUnique = new Set(['USD', 'GBP', 'USD', 'EUR', 'EUR']);
// console.log(currenciesUnique);
// // Key is the same as Value a Set does not have a key second value is redundant but third is still the Set
// currenciesUnique.forEach((val, _, set) => {
//   console.log(`${val}: ${val}`);
// })