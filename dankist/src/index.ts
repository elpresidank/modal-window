'use strict';
// Image Imports
// @ts-ignore
import Icon from './assets/img/icon.png';
// @ts-ignore
import Logo from './assets/img/logo.png';
// JS Imports
import accounts from './data';

// TYPES
interface Account {
  owner: string;
  movements: number[];
  interestRate: number;
  pin: number;
  username: string;
}


// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

const displayMovements = (movements: number[]) => {
  containerMovements.innerHTML = '';
  movements.forEach((mov, i) => {
    const transactionType = mov > 0 ? 'deposit' : 'withdrawal';
    const html = `
    <div class='movements__row'>
      <div class='movements__type movements__type--${transactionType}'>
        ${i + 1} ${transactionType}
      </div>
      <div class='movements__value'>${mov}€</div>
    </div>`;

    containerMovements.insertAdjacentHTML('afterbegin', html);

  });
};
displayMovements(accounts[0].movements);

const calcDisplayBalance = (movements: number[]) => {
  const balance = movements.reduce((acc, mov) => acc + mov, 0);
  labelBalance.textContent = `${balance} €`;
};
calcDisplayBalance(accounts[0].movements);

const calcDisplaySummary = (movements: number[]) => {
  const incomes = movements
    .filter(mov => mov > 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumIn.textContent = `${incomes}€`;

  const out = movements
    .filter(mov => mov < 0)
    .reduce((acc, mov) => acc + mov, 0);
  labelSumOut.textContent = `${Math.abs(out)}€`;

  const interest = movements
    .filter(mov => mov > 0)
    .map(deposit => deposit * 1.2/100)
    .filter((int, i, arr) => int >= 1)
    .reduce((acc, int) => acc + int, 0);
  labelSumInterest.textContent = `${interest}€`;

};
calcDisplaySummary(accounts[0].movements);

const createUsernames = (accs: Account[]) => {
  accs.forEach((acc: Account) => {
    acc.username = acc.owner
      .toLowerCase()
      .split(' ')
      .map(name => name[0])
      .join('');
  });
};
createUsernames(accounts);


