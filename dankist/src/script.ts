const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// const eurToUsd = 1.1;
//
// // map function returns an altered array
// const movementsUSD = movements.map(mov => mov * eurToUsd);
//
// console.log(movements);
// console.log(movementsUSD);
//
// // Does same as previous but with For of Loop
// const movementsUSDFor = [];
// for (const mov of movements) movementsUSDFor.push(mov * eurToUsd);
// console.log(movementsUSDFor);
//
// const movementsDescriptions = movements.map((mov, i) =>
//   `Movement ${i + 1}: You ${mov > 0 ? 'deposited' :
//     'withdrew'} ${Math.abs(
//     mov
//   )}`
// );
//
// console.log(movementsDescriptions)

// const deposits = movements.filter(mov => mov > 0);
// console.log(movements);
// console.log(deposits);
//
// // const depositsFor = [];
// // for (const mov of movements) if (mov > 0) depositsFor.push(mov);
// // console.log(depositsFor);
//
// const withdrawals = movements.filter(mov => mov < 0);
// console.log(withdrawals);

// console.log(movements);
//
// const balance = movements.reduce((acc, cur) => acc + cur, 0);
// console.log(balance);
//
// let balance2 = 0;
// for (const mov of movements) balance2 += mov;
// console.log(balance2);
//
// // Maximum value
// const max = movements.reduce((acc, mov) => acc > mov ? acc : mov, movements[0])
// console.log(max);

const euroToUsd = 1.1;
const totalDepositsUSD = movements
  .filter(mov => mov > 0)
  .map((mov, i, arr) => {
    // console.log(arr);
    return mov * euroToUsd;
  })
  .reduce((acc, mov) => acc + mov, 0);
console.log(totalDepositsUSD);